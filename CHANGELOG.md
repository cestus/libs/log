
# CHANGELOG

This CHANGELOG is a format conforming to [keep-a-changelog](https://github.com/olivierlacan/keep-a-changelog). 
It is generated with git-chglog -o CHANGELOG.md


<a name="v0.0.2"></a>
## [v0.0.2](https://gitlab.com/cestus/libs/log/compare/v0.0.1...v0.0.2)

### Feat

* add provider for ginkgo zap logs


<a name="v0.0.1"></a>
## v0.0.1

### Feat

* add log package

